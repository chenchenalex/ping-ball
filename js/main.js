(function() {
    var canvas = document.getElementById('canvas');
    if (!canvas.getContext) {
        return;
    }
    /*
    *************************
              Config       
    *************************
    */
    var ctx = canvas.getContext('2d'),
        colors = ['#FF7575', '#FFF475', '#75FFB3', '#75CCFF', '#9775FF', '#FF75C1', '#23D096', '#FFA824'],
        CANVAS_WIDTH = 800,
        CANVAS_HEIGHT = 600,
        RADIUS = 10,
        POINT_PER_HIT = 20,
        BRICK_SIZE = 43,
        BRICK_PER_LINE = Math.floor((CANVAS_WIDTH - 2 * 50) / BRICK_SIZE),
        DEAD_LINE = CANVAS_HEIGHT - 100,
        boxes = [],
        GAMESTART = false,
        score = 0,
        topScore = 0,
        timer = null,
        countDownSeconds = 20,
        req; //window.requestAnimationFrame

    var app = {
        gameover: function() {
            GAMESTART = false;
            score = 0;
            updateScore();
            counter.stop();
            clearCanvas();
            paintInit();
            cancelAnimationFrame(req);
        }
    };


    /*
    *************************
              Event       
    *************************
    */

    document.onkeydown = function(e) {
        var key = e.keyCode;

        if (key == 37 || key == 65) {
            board.moveLeft();
        } else if (key == 39 || key == 68) {
            board.moveRight();
        } else if (key == 38 || key == 87) {
            if (GAMESTART === false) {
                GAMESTART = true;
                if (window.requestAnimationFrame) {
                    var advance = function() {
                        clearCanvas();
                        board.updateBoard();
                        reloadBoxes();
                        ball.updateBall();
                        drawDeadLine();
                        if (GAMESTART) {
                            req = requestAnimationFrame(advance);
                        }
                    };
                    requestAnimationFrame(advance);
                }
                timer = counter.start();
            }
        }

        if (!GAMESTART) {
            ctx.clearRect(board.x, board.y - 2 * RADIUS - 4, 100, 2 * RADIUS + 4);
            ball.updateBall();
        }
    };

    /*
    *************************
      Functions and Objects      
    *************************
    */

    var board = {
        x: CANVAS_WIDTH / 2 - 50,
        y: CANVAS_HEIGHT - 50,
        width: 100,
        height: 50,
        BOARD_SPEED: 30,
        init: function() {
            this.paintBoard(this.x, this.y);
        },
        moveLeft: function() {
            if (this.x > 0) {
                if (this.x - this.BOARD_SPEED > 0) {
                    ctx.clearRect(board.x, board.y, board.width + 1, board.height);
                    this.updateBoard(-1);
                } else {
                    ctx.clearRect(board.x, board.y, board.width + 1, board.height);
                    this.x = 0;
                    this.paintBoard(this.x, this.y);
                }

            }
        },

        moveRight: function() {
            var rightEdge = this.x + this.width;
            if (rightEdge < CANVAS_WIDTH) {
                if (rightEdge + this.BOARD_SPEED <= CANVAS_WIDTH) {
                    ctx.clearRect(board.x - 1, board.y, board.width, board.height);
                    this.updateBoard(1);
                } else {
                    ctx.clearRect(board.x - 1, board.y, board.width, board.height);
                    this.x = CANVAS_WIDTH - this.width;
                    this.paintBoard(this.x, this.y);

                }
            }
        },

        updateBoard: function(d) {
            if (d < 0) {
                this.x -= this.BOARD_SPEED;
            } else if (d > 0) {
                this.x += this.BOARD_SPEED;
            }
            this.paintBoard(this.x, this.y);
        },

        paintBoard: function(x, y) {
            ctx.fillStyle = "#000";
            ctx.fillRect(x, y, 100, 20);
        }
    };

    var ball = {
        init: function() {
            this.x = board.x + 50;
            this.y = board.y - RADIUS;
            this.vx = Math.pow(-1, parseInt(Math.random() * 11)) * 3;
            this.vy = -4;

            this.paintBall(this.x, this.y);
        },

        updateBall: function() {

            if (GAMESTART) {
                var nx, ny, testResult;

                testResult = this.bumpTest(this.x, this.y, this.vx, this.vy);
                if (testResult.result === false) {
                    // if the ball did not hit walls, test blocks
                    testResult = this.hitboxTest(this.x, this.y, this.vx, this.vy);
                }

                if (testResult.vx !== 0) {
                    this.vx = testResult.vx;
                    this.vy = testResult.vy;
                } else {
                    return;
                }

                nx = this.x + this.vx;
                ny = this.y + this.vy;

                this.paintBall(nx, ny);

                this.x = nx;
                this.y = ny;

            } else {
                this.init();
            }
        },

        paintBall: function(x, y) {
            ctx.fillStyle = "#1E75A9";
            ctx.strokeStyle = "white";
            ctx.beginPath();
            ctx.arc(x, y, 10, 0, 2 * Math.PI);
            ctx.closePath();
            ctx.fill();
        },

        bumpTest: function(x, y, vx0, vy0) {
            var vx1 = vx0,
                vy1 = vy0,
                bump = true;

            // wall collision tests
            if (x - RADIUS > 0 && x + RADIUS < CANVAS_WIDTH && y - RADIUS > 0 && y + RADIUS < board.y) {
                return {
                    vx: vx1,
                    vy: vy1,
                    result: false
                };
            } else if (x - RADIUS <= 0 || x + RADIUS >= CANVAS_WIDTH) {
                //left and right walls
                vx1 = -1 * vx0;
            } else if (y - RADIUS <= 0) {
                //ceiling
                vy1 = -1 * vy0;
            } else if (y + RADIUS > board.y && y < board.y + 3 && x - RADIUS >= board.x && x + RADIUS <= board.x + 100) {
                //board
                vy1 = -1 * vy0;
            } else if (y + RADIUS > board.y && y < board.y + 3 && x + RADIUS > board.x && x - RADIUS < board.x && vx0 > 0) {
                //board left edge
                vy1 = -1 * vy0;
                vx1 = -1 * vx0;
            } else if (y + RADIUS > board.y && y < board.y + 3 && x - RADIUS < board.x + 100 && x + RADIUS > board.x + 100 && vx0 < 0) {
                //board right edge
                vy1 = -1 * vy0;
                vx1 = -1 * vx0;
            } else if (y + RADIUS >= CANVAS_HEIGHT) {
                //game over
                app.gameover();
                return {
                    vx: 0,
                    vy: 0
                };

            }

            //return calculation value
            return {
                vx: vx1,
                vy: vy1,
                result: bump
            };
        },

        hitboxTest: function(x, y, vx0, vy0) {
            var i = 0,
                vx1 = vx0,
                vy1 = vy0;

            for (i = 0; i < boxes.length; i++) {
                var item = boxes[i];

                // no collision
                if (y + RADIUS < item.y) continue; //top
                else if (y - RADIUS > item.y + BRICK_SIZE) continue; //bottom
                else if (x + RADIUS < item.x) continue; //left
                else if (x - RADIUS > item.x + BRICK_SIZE) continue; //right

                //hit direction
                if (vx0 > 0 && vy0 > 0) {
                    if (y < item.y) {
                        //hit top
                        vy1 = -1 * vy0;
                    } else {
                        //hit left
                        vx1 = -1 * vx0;
                    }
                } else if (vx0 > 0 && vy0 < 0) {
                    if (y > item.y + BRICK_SIZE) {
                        //hit bottom
                        vy1 = -1 * vy0;
                    } else {
                        //hit left
                        vx1 = -1 * vx0;
                    }
                } else if (vx0 < 0 && vy0 > 0) {
                    if (y < item.y) {
                        //hit top
                        vy1 = -1 * vy0;
                    } else {
                        //hit right
                        vx1 = -1 * vx0;
                    }
                } else {
                    if (y > item.y + BRICK_SIZE) {
                        //hit bottom
                        vy1 = -1 * vy0;
                    } else {
                        //hit right
                        vx1 = -1 * vx0;
                    }
                }
                boxes.splice(i, 1);
                score += POINT_PER_HIT;
                updateScore();

            }

            return {
                vx: vx1,
                vy: vy1
            };
        }
    };

    function updateBoxes() {
        var tempBox, i;

        boxes.forEach(function(item) {
            item.y += BRICK_SIZE;
        });

        // add a new line of bricks
        for (i = 0; i < BRICK_PER_LINE; i++) {
            var clr = colors[Math.floor(Math.random() * colors.length)];
            tempBox = {
                x: 50 + i * BRICK_SIZE,
                y: 50,
                color: clr
            };
            boxes.push(tempBox);
        }
        
        timer = counter.start();
        reloadBoxes();

    }

    function reloadBoxes() {
        var bottomBrick = 0;
        boxes.forEach(function(item) {
            paintBox(item.x, item.y, item.color);
            bottomBrick = item.y + BRICK_SIZE > bottomBrick ? (item.y + BRICK_SIZE) : bottomBrick;
        });
        if (bottomBrick > DEAD_LINE) app.gameover();
    }

    var counter = {
        t: countDownSeconds,

        start: function() {
            var countDown = document.getElementById('counter'),
                self = this;

            return setInterval(function() {
                if (self.t > 0) {
                    self.t -= 1;
                    countDown.innerHTML = self.t;
                } else {
                    self.stop();
                    updateBoxes();
                }
            }, 1000);
        },

        stop: function() {
            this.t = countDownSeconds;
            clearInterval(timer);
            document.getElementById('counter').innerHTML = this.t;
            timer = null;
        }
    };

    function paintInit() {
        boxes = [];


        //init boxes
        for (var i = 0; i < 4; i++) {
            for (var j = 0; j < BRICK_PER_LINE; j++) {
                var clr = colors[Math.floor(Math.random() * colors.length)],
                    tempBox = {
                        x: 50 + j * BRICK_SIZE,
                        y: 50 + i * BRICK_SIZE,
                        color: clr
                    };
                paintBox(tempBox.x, tempBox.y, clr);
                boxes.push(tempBox);
            }
        }
        drawDeadLine();
        board.init();
        ball.init();
    }

    function drawDeadLine() {
        ctx.strokeStyle = "#B70000";
        ctx.beginPath();
        ctx.moveTo(0, DEAD_LINE);
        ctx.lineTo(CANVAS_WIDTH, DEAD_LINE);
        ctx.closePath();
        ctx.stroke();

    }

    function paintBox(x, y, clr) {
        ctx.strokeStyle = 'black';
        ctx.fillStyle = clr;
        ctx.fillRect(x, y, BRICK_SIZE, BRICK_SIZE);
        ctx.strokeRect(x, y, BRICK_SIZE, BRICK_SIZE);
    }

    function clearCanvas() {
        ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
    }

    function updateScore() {
        var scoreBoard = document.getElementById('score-value'),
            topScoreBoard = document.getElementById('top-score');

        if (score > topScore) {
            topScore = score;
        }

        scoreBoard.innerHTML = score;
        topScoreBoard.innerHTML = topScore;
    }

    paintInit();
    updateScore();
})();
